package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto registraProduto(@RequestBody Produto produto)
    {
        return produtoService.salvarProduto(produto);

    }

    @GetMapping
    public Iterable<Produto> buscarProdutos()
    {
        return produtoService.buscarTodosProdutos();
    }

}
